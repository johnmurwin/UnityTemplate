<!--
*** UNIVERSAL README TEMPLATE 

*** UPDATED: 07/28/2022
*** BY: John Murwin
-->

<!--
*** To avoid retyping too much info. Do a search and replace for the following:
*** github_username, repo_link, repo_name, twitter_handle, email, project_title, project_description, sonar_project_key
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->


<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="repo_link">
    <img src="docs/logo.png" alt="Logo" width="517.5" height="122.1">
  </a>
</p>

<!-- SONAR SHIELDS -->
<p align="center">
  <a>
    <img src="sonar_qualityGate">
    <img src="sonar_bugs">
    <img src="sonar_codeSmells">
    <img src="sonar_vulnerabilities">
    <img src="sonar_coverage">
  </a>
</p>
<p align="center">
  <a>
    <img src="sonar_maintainability">
    <img src="sonar_reliability">
    <img src="sonar_security">
    <img src="sonar_technicalDebt">
  </a>
</p>

<p align="center">
This repo serves as a Template Repository that provides quick access to a Universal Rendering Pipeline supported Unity project with a set of standardized assets, tools, and references with configuration for Unity Test Runner for Unit and Integration Tests, Code Coverage Support, and a fully automated Continous Improvement and Deployment pipelines utilizing Gitlab Runner. 
    <br />
    <a href="doc_link"><strong>Explore the Docs »</strong></a>
    <a href="project_license"><strong>Project License »</strong></a>
    <a href="contributor_convenent"><strong>Contribute to the Project »</strong></a>
    <br />
    <br />
    <a href="release_link">Latest Release</a>
    ·
    <a href="bug_report">Report Bug</a>
    ·
    <a href="feature_request">Request Feature</a>
    ·
    <a href="discussion_link">Start Discussion</a>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
### Description
This project is a **Unity 2021.3+** Template project that is configured for my base need set. **Universal Rendering Pipeline** is enabled and configured with a base set of quality profiles to handle the majority of my *non-HDRP/VR* projects.

A handful of stock Unity Plugins have been included (see Key Features for more details), along with a handful of 'nice to have' store assets to make development of almost every game incredibly easy.

This template will be updated as my version of choice changes, and the accompanying assets kept up to date where possible. See contact information for issues or support.


### Key Features:
#### Unity Packages:
- Editor Support: Rider, VSCode, and Visual Studio

#### Asset Store Items:
- TBD

### Built With
* [Unity](https://unity.com)
* [Gitlab](https://www.gitlab.com)
* [Jetbrains Rider](https://jetbrains.com/rider)
* [SonarLint](sonarlint.org)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites
* Source Control Tool (GitKraken)
* Script Editor (Rider / VSCode)

### Installation

1. Clone the repo
```shell
git clone https://gitlab.com/johnmurwin/UnityTemplate.git
```

2. Open project in Unity


3. Create a _Dev Folder and a dev-level based off the 'DevScene' template scene with your name
```shell
ex: Assets/ProjectName/_Dev/DevJohn/DevJohn.scene
```

4. Submit a PR for your DevLevel


<!-- CONTRIBUTING -->
## Branch Workflow

This project utilizes the [Trunk-Based](https://www.atlassian.com/continuous-delivery/continuous-integration/trunk-based-development#:~:text=Trunk%2Dbased%20development%20is%20a,software%20delivery%20and%20organizational%20performance) methodology for branch workflows. The basic workflow is as follows:

1. Clone the Project
2. Create your Feature Branch (`git checkout -b AmazingFeature`)
3. Commit your Changes (`git commit -m 'convID: add some amazing feature'`)
4. Push to the Branch (`git push origin AmazingFeature`)
5. Open a Pull Request using PR Template

<!-- USAGE EXAMPLES -->
## Usage

Follows usage guidelines set forth by the Project License and the Contributor Guidelines.


<!-- STYLE EXAMPLES -->
## Project Style Guide
For examples, please refer to the Fathom 7 Style Guide: [F7 Engine Style Guide](https://github.com/Truly-Integrated-Computing/Knowledgebase/wiki/F7-Engine-Style-Guide)   
_Partially based off design structures and components from [here](https://github.com/justinwasilenko/Unity-Style-Guide)._


<!-- ROADMAP -->
## Roadmap

See the [Roadmap](https://trulyintegrated.atlassian.net/jira/software/c/projects/UnityTempalate/boards/24/roadmap) for a list of milestones, features, and known issues.

<!-- LICENSE -->
## License

See `LICENSE.md` for more information.

<!-- CONTACT -->
## Contact

John Murwin - [@JohnMurwin](https://twitter.com/JohnMurwin) - john.murwin@gmail.com   
Project Website: [www.todo.com](http://todo.com/)  
Project Link: [www.todo.com](https://www.todo.com)


<!-- ACKNOWLEDGEMENTS -->
### Acknowledgements & Thanks

* [@Othneildrew](https://github.com/othneildrew)
* [@stevemao](https://github.com/stevemao)
* [@Allar](https://github.com/Allar)


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/Truly-Integrated-Computing/repo.svg?style=for-the-badge
[contributors-url]: https://github.com/Truly-Integrated-Computing/UnityTemplate/people
